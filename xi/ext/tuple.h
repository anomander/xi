#pragma once

#include <tuple>

#include "xi/ext/common.h"
#include "xi/ext/meta.h"

namespace xi {
inline namespace ext {

  using ::std::tuple;
  using ::std::tuple_size;
  using ::std::make_tuple;
}
}
