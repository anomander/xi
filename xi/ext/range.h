#pragma once

#include <boost/range/adaptor/reversed.hpp>

namespace xi {
inline namespace ext {
  namespace adaptors {
    using ::boost::adaptors::reverse;
  } // namespace adaptors
} // inline namespace ext
} // namespace xi
